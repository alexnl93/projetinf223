#include "library.h"


Library::Library(){}

//creator of Media and Groupe (grep)
std::shared_ptr<Photo> Library::createPhoto(std::string name, std::string path,
                            int mylongitude, int mylatitude) {


    Photo *myPhoto = new Photo(name,path,mylongitude,mylatitude);
    std::shared_ptr<Photo> photo(myPhoto);
    std::string key= name;
    map_Media[key]=photo;
    return photo;

}


std::shared_ptr<Video> Library::createVideo(std::string name, std::string path,int myduree) {

    Video *myVideo = new Video(name,path,myduree);
    std::shared_ptr<Video> video(myVideo);
    std::string key= name;
    map_Media[key]=video;
    return video;

}


std::shared_ptr<Film> Library::createFilm(std::string name, std::string path,
                           int duree, int mytaille, int *mydrueeChapitres) {

    Film *myFilm = new Film(name,path,duree);
    std::shared_ptr<Film> film(myFilm);
    myFilm->setDureeChapitres(mytaille,mydrueeChapitres);
    if ( map_Media.find(name) != map_Media.end()){
        remove(name);
    }

    std::string key= name;
    map_Media[key]=film;
    return film;
}

std::shared_ptr<Groupe> Library::createGroupe(std::string name) {

    Groupe * myGroupe = new Groupe(name);
    ptr_Groupe groupe(myGroupe);
    std::string key= name;
    map_Groupe[key]=groupe;
    return groupe;
}

//to remove an element
void Library::remove(std::string name) {
    Map_Media::iterator itMedia ;
    Map_Groupe::iterator itGroupe ;


    itMedia = map_Media.find(name);
    if(itMedia != map_Media.end()){
        std::shared_ptr<Media> ptr = map_Media[name] ;
        map_Media.erase(itMedia);
        ptr.reset();
    }

    itGroupe = map_Groupe.find(name);
    if(itGroupe != map_Groupe.end()){
        std::shared_ptr<Groupe> ptr = map_Groupe[name] ;
        map_Groupe.erase(itGroupe);
        ptr.reset();
    }
}

//lookafter an element, utiliser display()
void Library::research(std::string name,std::ostream& s)  {

    bool found = false ;

    if(map_Media.find(name) != map_Media.end()){
        ptr_Media tampon = map_Media[name];
        tampon->display(s);
        found = true ;
    }

    if(map_Groupe.find(name) != map_Groupe.end()){
         s <<""<< name <<"\n";
         map_Groupe[name]->display(s);
         found = true ;
    }

    if ( ! found)  s <<"itm ["<< name <<"] non trouvé !\n";

}

//play a media,same thing that research but with play
void Library::play(std::string name) {

    if(map_Media.find(name) != map_Media.end()){
        map_Media[name]->play();
    }

}
