#ifndef LIBRARY_H
#define LIBRARY_H
#include <iostream>
#include <string>
#include <memory>
#include <map>
#include "media.h"
#include "photo.h"
#include "video.h"
#include "film.h"
#include "groupe.h"

typedef std::shared_ptr<Groupe> ptr_Groupe;
typedef std::map<std::string, ptr_Groupe> Map_Groupe;
typedef std::map<std::string, ptr_Media> Map_Media;

class Library {

private:
    Map_Media map_Media;
    Map_Groupe map_Groupe;

public:
    Library();

    //creator of Media and Groupe (grep)
    virtual std::shared_ptr<Photo> createPhoto(std::string name, std::string path,
                                int mylongitude, int mylatitude) ;
    virtual std::shared_ptr<Video> createVideo(std::string name, std::string path,int myduree) ;
    virtual std::shared_ptr<Film> createFilm(std::string name, std::string path,
                               int duree, int mytaille, int *mydrueeChapitres) ;
    virtual std::shared_ptr<Groupe> createGroupe(std::string name) ;

    //to remove an element
    virtual void remove(std::string name) ;

    //lookafter an element, utiliser display()
    virtual void research(std::string name,std::ostream& s) ;

    //play a media,same thing that research but with play
    virtual void play(std::string name) ;




};

#endif // LIBRARY_H
