#ifndef VIDEO_H
#define VIDEO_H
#include <string>
#include <iostream>
#include "media.h"

class Video : public Media {

private:
    int duree;

public:
    Video(std::string name, std::string path,int myduree):
        Media(name,path),duree(myduree){} // longitude(mylongitude) <=> {longitude = mylongitude }

    Video() :Media(){}

    virtual ~Video(){}

    virtual void setDuree(int myduree){duree= myduree;}

    virtual int getDuree() const {return duree;}

    virtual void display (std::ostream& s) const
    {s << this->getName() <<' '<< this->getPath() <<' '<< this->getDuree() <<std::endl;}

    virtual void play() const
    {std::string s="mpv " + getPath()+"&"; system(s.c_str());}
};


#endif // VIDEO_H
