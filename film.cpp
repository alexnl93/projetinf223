#include "film.h"
#include <string>
using namespace std;



Film::Film():Video(), nbChapitres(0), dureeChapitres(NULL) {}

Film::Film(std::string name, std::string path,int duree)
    : Video(name,path,duree), nbChapitres(0), dureeChapitres(NULL) {
}

Film::Film(std::string name, std::string path,int duree, int mytaille, int *mydureeChapitres)
    :Video(name,path,duree){

    nbChapitres = mytaille;
    dureeChapitres = new int[nbChapitres];
    for (int i = 0; i < nbChapitres; i++){
        dureeChapitres[i] = mydureeChapitres[i];
    }
}


Film::~Film(){
    delete[] dureeChapitres;
    dureeChapitres = NULL;
}

void Film::setDureeChapitres(int mytaille, const int *mydureeChapitres)
{
    nbChapitres = mytaille;
    delete[] dureeChapitres;
    dureeChapitres = new int[nbChapitres];
    for (int i = 0; i < nbChapitres; i++){
        dureeChapitres[i] = mydureeChapitres[i];
    }
}

const int Film::getNbChapitres() const{
    return nbChapitres;
}

const int Film::getDureeChapitres() const
{
    return dureeChapitres[0];
}

void Film::display(std::ostream& s) const{


    this->Video::display(s);
    s << "film : [" << this->getName() << "] nombre de chapitre : "<< nbChapitres <<std::endl;
    for (int i = 0; i < nbChapitres; i++){
        s << "     Durée du chapitre numéro " << i+1 << ':' <<' '<< this->dureeChapitres[i] <<std::endl ;
    }
    s <<std::endl;
}

void Film::displayChapter (std::ostream& s) const {

    for (int i = 0; i < nbChapitres; i++){
        s << "voici la durée du chapitre numéro " << i << ':' <<' '<< this->dureeChapitres[i] <<std::endl;

    }
}
