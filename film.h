#ifndef FILM_H
#define FILM_H
#include <iostream>
#include <string>
#include "video.h"

class Film : public Video {

private :
    int nbChapitres;
    int * dureeChapitres;

public:
    Film();
    Film(std::string name, std::string path,int duree);
    Film(std::string name, std::string path,int duree, int mytaille, int *mydrueeChapitres);

    virtual ~Film();

    virtual void setDureeChapitres(int mytaille, const int *mydrueeChapitres);

    virtual const int getDureeChapitres() const;

    virtual const int getNbChapitres() const;

    virtual void display(std::ostream& s) const;

    virtual void displayChapter (std::ostream& s) const;

};

#endif // FILM_H
