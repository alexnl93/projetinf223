#include "media.h"
#include "photo.h"
#include "video.h"
#include "film.h"
#include "groupe.h"
#include "library.h"
#include "TCPServer.h"
#include <memory>
#include <list>

using namespace std;
typedef std::map<int, std::string> Map_String;
const int DEFAULT_PORT = 3331;

Library *itunesAlex = new Library();


class MyApp {
public:

  /// Cette fonction est appelée chaque fois qu'il y a une requête à traiter.
  /// - 'request' contient la requête
  /// - 'response' sert à indiquer la réponse qui sera renvoyée au client
  /// - si la fonction renvoie false la connexion est close.
  //
  // Cette fonction peut etre appelée en parallele par plusieurs threads (il y a
  // un thread par client).
  // Si le verrou 'lock' est bloqué en mode WRITE, les autres appels sont bloqués
  // jusqu'à la fin l'appel qui a bloqué le verrou.
  //
  bool processRequest(TCPServer::Cnx& cnx, const string& request, string& response)
  {
    // mettre cette variable à true si la commande modifie les donnees du programme
    bool changeData = false;
    if (request == "delMedias" || request == "delGroups") changeData = true;

    // suivant le cas on bloque le verrou en mode WRITE ou en mode READ
    TCPServer::Lock lock(cnx, changeData);

    cerr << "request: '" << request << "'" << endl;

    Map_String map_String;
    std::string phrase;
    phrase = request;
    size_t pos = 0;
    std::string delimiter = " ";
    std::string token;
    int i =0;

    while ((pos = phrase.find(delimiter)) != std::string::npos) {
        i = i + 1;
        token = phrase.substr(0, pos);
        map_String[i]= token;
        std::cout << token << std::endl;
        phrase.erase(0, pos + delimiter.length());
    }
    i = i + 1;
    std::cout << phrase << std::endl;
    map_String[i]= phrase;

    if (map_String.size() != 2) {

        std::cout << "instruction de longueur incorrect" << std::endl;
        response= "instruction de longueur incorrect";
        cerr << "response: '" << response << "'" << endl;
    }else{

    std::string nameMedia = map_String[2];
    if (map_String[1]== "research"){
        itunesAlex->research(nameMedia,std::cerr);
        std::cout << "done" << std::endl;
        response= "done";
        cerr << "response: '" << response << "'" << endl;
    }else{

    if (map_String[1]== "display"){
        itunesAlex->research(nameMedia,cerr);
        std::cout << "done1" << std::endl;
        response= "done2";
        cerr << "response: '" << response << "'" << endl;
    }else{

    if (map_String[1]== "play"){
        itunesAlex->play(nameMedia);
        std::cout << "done" << std::endl;
        response= "La lecture a commencé";
        cerr << "response: '" << response << "'" << endl;
    }else{

        std::cout << "nom instruction incorrect" << std::endl;
        response= "nom instruction incorrect";
        cerr << "response: '" << response << "'" << endl;

    }
    }
    }
    }
    // simule un traitement long (décommenter pour tester le verrou)
    // if (changeData) sleep(10); else sleep(5);

    /*cerr << "response: '" << response << "'" << endl;*/

    // renvoyer false pour clore la connexion avec le client
    return true;
  }
};


int main(int argc, char* argv[])
{

  int * tampon1= new int[2];
  tampon1[0] = 20;
  tampon1[1] = 25;

  int * tampon2= new int[5];
  tampon2[0] = 30;
  tampon2[1] = 29;
  tampon2[2] = 28;
  tampon2[3] = 27;
  tampon2[4] = 26;

  shared_ptr<Groupe> g= itunesAlex->createGroupe("GroupePhoto1");
  itunesAlex->createGroupe("GroupeVideo1");
  itunesAlex->createGroupe("GroupeFilm1");

  itunesAlex->createPhoto("Photo1", "path1",580,320);
  itunesAlex->createPhoto("Photo1", "path1",320,580);
  itunesAlex->createPhoto("Photo2", "path1",580,320);
  itunesAlex->createPhoto("Photo3", "path2",1920,1080);
  // g.push_back(p1);

  itunesAlex->createVideo("Video1", "path1",20);
  itunesAlex->createVideo("Video2", "path2",30);
  itunesAlex->createFilm("Film1","path2",45,2,tampon1);

  // (itunesAlex->create("Film2","path2",30,3,tampon2)).display(cout);


  itunesAlex->createFilm("Film2","path2",30,5,tampon2);


  TCPServer * server = new TCPServer();
  MyApp * app = new MyApp();
  server->setCallback(app, &MyApp::processRequest);

  int port = (argc >= 2) ? atoi(argv[1]) : DEFAULT_PORT;
  cout << "Starting Server on port " << port << endl;
  int status = server->run(port);

  if (status < 0) {
    cerr << "Could not start Server on port " << port << endl;
    return 1;
  }
  else return 0;
}


