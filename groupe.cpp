#include "groupe.h"
using namespace std;

Groupe::Groupe(string myName)
{
    name = myName;

}

Groupe::Groupe(){}

Groupe::~Groupe(){}
/* Pas besoin de mettrer un delete car
 * son seul attribut n'est pas un pointeur
 * donc de fais pas un new*/

void Groupe::setName (string myname){
    name = myname;
}

string Groupe::getName()const{
    return (name);
}

void Groupe::display(std::ostream& s) const{

    for(auto it: *this) {

        /*Vue que displaye est une classe abstraite,
         * on ne peut pas l'appeler directement sur
         * media, on est donc obliger de faire un
         * case en fonction du type*/

        (it)->display(s);
        //ne pas oublier d luir repasser les paramètres
    }
}
