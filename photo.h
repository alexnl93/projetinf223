#ifndef PHOTO_H
#define PHOTO_H
#include <string>
#include <iostream>
#include "media.h"

class Photo : public Media {

private:
    int longitude, latitude;

public:
    Photo(std::string name, std::string path,int mylongitude, int mylatitude):
        Media(name,path),longitude(mylongitude),latitude(mylatitude){} // longitude(mylongitude) <=> {longitude = mylongitude }

    virtual ~Photo(){}

    virtual void setLongitude(int mylongitude){longitude = mylongitude;}
    virtual void setLatitude(int mylatitude){latitude = mylatitude;}

    virtual int getLongitude() const {return longitude;}
    virtual int getLatitude() const {return latitude;}

    virtual void display (std::ostream& s) const
    {s << this->getName() <<' '<< this->getPath() <<' '<< this->getLongitude() <<' '<< this->getLatitude() <<std::endl;}

    virtual void play() const
    {std::string s="imagej " + getPath()+"&"; system(s.c_str());}
};

#endif // PHOTO_H
