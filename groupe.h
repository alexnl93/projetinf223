#ifndef GROUPE_H
#define GROUPE_H
#include <iostream>
#include <string>
#include <list>
#include <memory>
#include "media.h"



//Deuxième possibilitée

typedef std::shared_ptr<Media> ptr_Media;

class Groupe : public std::list<ptr_Media> {

private:

    //C'est une liste de pointeur!!
    std::string name;



public:
    Groupe();
    Groupe(std::string myName);

    virtual ~Groupe();

    virtual void setName (std::string);
    virtual std::string getName () const;

    virtual void display(std::ostream&)const;

};

/*
class Groupe {

private:
    //C'est une liste de pointeur!!
    list<Media*> mediaList;
    std::string name;

public:
    Groupe();

};*/


#endif // GROUPE_H

