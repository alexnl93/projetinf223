#ifndef MEDIA_H
#define MEDIA_H
#include <iostream>
#include <string>
#include <memory>


class Media
{
public:
    std::string name, path;

public:
    Media();
    Media(std::string, std::string);

    virtual ~Media();

    virtual void setName (std::string);
    virtual void setPath (std::string);

    virtual std::string getName () const;
    virtual std::string getPath () const;

    virtual void display (std::ostream&) const ;

    virtual void play() const = 0;
};

#endif // MEDIA_H
