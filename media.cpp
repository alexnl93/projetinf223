#include "media.h"
#include <string>
using namespace std;


Media::Media(string myname, string mypath)
{
    name = myname;
    path = mypath;

}

Media::Media(){

}

void Media::setName (string myname){
    name = myname;
}

void Media::setPath (string mypath){
    path = mypath ;
}

string Media::getName()const{
    return (name);
}
string Media::getPath()const{
    return(path);
}

Media::~Media(){

}

void Media::display(ostream & s)const{
    s << this->getName() << " ["<< this->getPath() << ']' << endl;
}
